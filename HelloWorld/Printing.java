package Java.Learning.Pt1HelloWorld;

public class Printing {
    public static void main(String[] args) {
        System.out.println("This is printing a line on its own.");
        System.out.print("This is");
        System.out.print("printing as one singular line!");
    }
}
