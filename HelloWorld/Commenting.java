package Java.Learning.Pt1HelloWorld;

public class Commenting {
    
}

//The double-slash is the single line syntax for short comments.

/*
 * If the comment is a long one, use this! To begin, use a forward-slash
 * and an asterisk.
 */

 /**
  * This is an alternative way of doing it, with a forward-slash and
  two asterisks. These are Javadoc comments, and are used to make
  documentation for APIs.

  They're typically written before the declaration of fields, methods
  and classes.
  */

  // Comments won't show in a program as they aren't executable code.