package Java.Learning.Variables;
/*
 * This example uses bank account balances (my own at the time of writing) to do this.
 * I can use expressions, arithmetic operators and others to change the values of
 * the variables.
 */

public class ManipulateVariables {
    public static void main(String[] args) {
        //Initial balance
        double balance = 329.99;
        //Deposit amount declared
        double depositAmount = 20.00;
        // Store result of calculation in original balance variable
        balance = balance + depositAmount;
        System.out.println(balance);

        //Say I want to get that £20 out...
        double withdrawAmount = 20.00;
        balance = balance - withdrawAmount;
        System.out.println(balance);
        //Now it's back to £329.99

        //Works with int's too
        int numOfCameras = 11 + 1;
        System.out.println(numOfCameras);

        //What if I bought ANOTHER camera
        numOfCameras++;
        System.out.println(numOfCameras);

        //I can multiply too - 40 * 13.50 is £540
        double moneyMoneyAmount = 40 * 13.50;

        //I can also divide!
        double divExample = 400;
        double divExample2 = divExample / 4;
        System.out.println(divExample2);

        //With integers though any remainder is lost
        int evenlyDivided = 10 / 5;
        System.out.println(evenlyDivided);
        int unevenlyDivided = 10 / 4;
        System.out.println(unevenlyDivided);
        System.out.println("The second '2' should have been 2.5 because 10 / 4 is 2.5");

    }
}

/*
 * Don't ever try to divide by zero. You'll get an ArithmeticException.
 */