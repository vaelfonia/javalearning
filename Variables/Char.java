package Java.Learning.Variables;

public class Char {
    public static void main(String [] args) {
        char grade = '8';
        char firstLetter = 'a';
        char punctuation = '!';
    }
}

/*
 * These are char's. They can hold any single character. Has to be
 * surrounded by single quotations. 'these.'
 */