package Java.Learning.Variables;

public class finalKeyword {
    public static void main(String[] args) {
        //To declare a variable with a value that CANNOT be manipulated, use the final keyword.
        //Prepend it to a variable declaration. E.g...

        final int yearOfBirth = 2005;

        //Any attempts to change this variable will throw an error.

        yearOfBirth = 2005-1;
    }
}
