package Java.Learning.Variables;

public class OrderOfOps {
    public static void main(String[] args) {
        /*
         * The order of operations for the Java compilder is as follows:
         * - Parentheses (brackets)
         * - Exponents
         * - Modulo, Multiplication or Division
         * - Addition or Subtraction
         */

        //Take this expression
        int num = 5 * (10 - 4) + 4 / 2;

        /*
         * The 10 - 4 would be evaluated first because it's in brackets / parentheses.
         * It would become 6, turning the expression into this:
         * 
         * 5 * 6 + 4 / 2
         * 
         * Next the 5 * 6 would be evaluated because it's multiplication.
         * It turns into 30. The expression now looks like this:
         * 
         * 30 + 4 / 2
         * 
         * Next in the order, 4 / 2 will get evaluated because division has higher
         * importance compared to addition. It now turns into this:
         * 
         * 30 + 2
         * 
         * The sum of this is 32. This means the value of num is 32.
         */
    }
}
