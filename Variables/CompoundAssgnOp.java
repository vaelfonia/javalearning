package Java.Learning.Variables;

public class CompoundAssgnOp {
    public static void main(String[] args) {
        /*
         * Sometimes, you have to adjust the value of a variable.
         * Compund assignment operators help by shortening the syntax.
         * You can use addition, subtraction, multiplication, divisiona and modulo
         * here. +=, -=, *=, /= and %=.
         */

        //Without CAO
        int numCupcakes = 12;
        numCupcakes = numCupcakes + 8;

        //With CAO
        numCupcakes +=8;

        //These produce the same result, but it's written faster and ref'd once.
    }
}
