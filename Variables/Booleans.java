package Java.Learning.Variables;

public class Booleans {
    public static void main(String[] args) {
        boolean javaIsPainPeko = true;
        boolean javaIsFunPeko = false;

        System.out.println(javaIsPainPeko);
    }
}

/*
 * These are booleans, variables that can be yes or no, true or false.
 */