package Java.Learning.Variables;

public class equals {
    public static void main(String[] args) {
      /*
       * To test equality with objects, you can use a built in method called .equals().
       * Always use this when comparing OBJECTS. == could work occasionally but the reason why
       * it doesn't work most of the time is to do with how objects are stored in memory.
       */
      String person1 = "Vael";
      String person2 = "Jerry";
      String person3 = "Lofty";
      String person4 = "Vael";

      System.out.println(person1.equals(person2));
      System.out.println(person1.equals(person4));
    }
}
