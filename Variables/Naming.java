package Java.Learning.Variables;

public class Naming {
    public static void main(String[] args) {
     String firstName = "Vael";
     String secondName = "Relanah";
     String emailAddr = "vael@email.xyz";
     int salaryExpectation = 18000;
     int yrofBirth = 2005;

    }
}

/*
 * Naming convention is important, future Vael!
 * 
 * Use camel case (like this: camelCase) to name variables.
 * They can start with any letter, a $ or an _ (underscore), anything else
 * is illegal. Also, variable names are case sensitive.
 */
