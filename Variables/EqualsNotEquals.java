package Java.Learning.Variables;

public class EqualsNotEquals {
    public static void main(String[] args) {
        /*
         * Another relational operator is ==. This will tell you if two variables are equal.
         * To check if two variables are NOT equal, you use !=.
         */

        double paycheckAmount = 620;
        double calculatedPaycheck = 15.50 * 40;

        System.out.println(paycheckAmount == calculatedPaycheck);

        double balance = 329.99;
        double depositAmount = 620;
        double updatedBalance = balance + depositAmount;

        boolean balancedChanged = balance != updatedBalance;
        System.out.println(balancedChanged);

        //Greator/Less Than or Equal To

        System.out.println(paycheckAmount >= calculatedPaycheck);
    }
}
