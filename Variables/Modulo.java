package Java.Learning.Variables;

public class Modulo {
    public static void main(String[] args) {
        /*
         * The Modulo operator will give you the remainder after two numbers
         * are divided.
         */

        int muffinsBaked = 10;
        int muffinsLeftOver = muffinsBaked % 3;
        //muffinsLeftOver now holds 1
        System.out.println("Number of muffins left over:");
        System.out.println(muffinsLeftOver);
    }
}
