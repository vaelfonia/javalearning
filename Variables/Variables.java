package Java.Learning.Variables;

public class Variables {
    public static void main(String[] args) {
        String name = "James Gosling";
        int yearCreated = 1995;

        System.out.println(name);

        System.out.println(yearCreated);
    }
}

/*
 * Here are variables. You have to declare them in main.
 * 
 * Strings are strings of text, as shown.
 * Ints are integers, so, whole numbers.
 * You have to give these variables names.
 * 
 * You can also make Java print these variables out by typing them
 * without quotation marks in print or println calls.
 */