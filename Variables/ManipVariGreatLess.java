package Java.Learning.Variables;

/*
 * Using bank balances again, we're using greater than (>) and less than (<) symbols.
 * They make boolean comparisons.
 */

public class ManipVariGreatLess {
    public static void main(String[] args) {
        double balance = 329.99;
        double amountToWithdraw = 38.50;
        System.out.print(amountToWithdraw < balance);

        double costOfNewLaptop = 1299.00;
        boolean canBuyLaptop = balance > costOfNewLaptop;
        System.out.println(canBuyLaptop);
    }
}

/*
 * Don't ever try to divide by zero. You'll get an ArithmeticException.
 */