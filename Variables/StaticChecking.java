package Java.Learning.Variables;

public class StaticChecking {
    public static void main(String[] args) {
    String year = 2001;
    double title = "Shrek";
    int genre = 'C';
    boolean runtime = 1.58;
    char isPG = true;
}

/*
 * Java has static typing, and won't compile the program if is a variable
 * is assigned a value of an incorrect type.
 * 
 * For example, an int cannot be a letter, nor can a boolean be a decimal
 * number. Nor can a char be true or false. Nor can a double be a word.
 */