package Java.Learning.Variables;

public class StringConcentration {
    public static void main(String[] args) {
        /*
         * A few arithmetic operators also work on Strings too, such as +.
         */

        String username = "vaelfonia";
        System.out.println("Your username is: " + username);

        //This works with other data types too, such as integers or doubles.

        double balance = 329.99;
        String message = "Your balance is: " + balance;
        System.out.println(message);
    }
}
